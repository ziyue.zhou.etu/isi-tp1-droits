#!/bin/bash

adduser administer
adduser lambda_a
adduser lambda_b

addgroup group_a
addgroup group_b
addgroup group_c

adduser lambda_a group_a
adduser lambda_a group_c
adduser lambda_b group_b
adduser lambda_b group_c
adduser administer group_a
adduser administer group_b
adduser administer group_c

mkdir dir_a dir_b dir_c
touch dir_a/file1 dir_a/file2 dir_a/file3
echo -e "hello,i'm dir_a/file1" >> dir_a/file1
echo -e "hello,i'm dir_a/file2" >> dir_a/file2
echo -e "hello,i'm dir_a/file3" >> dir_a/file3
touch dir_b/file1 dir_b/file2 dir_b/file3
echo -e "hello,i'm dir_b/file1" >> dir_b/file1
echo -e "hello,i'm dir_b/file2" >> dir_b/file2
echo -e "hello,i'm dir_b/file3" >> dir_b/file3
touch dir_c/file1 dir_c/file2 dir_c/file3
echo -e "hello,i'm dir_c/file1" >> dir_c/file1
echo -e "hello,i'm dir_c/file2" >> dir_c/file2
echo -e "hello,i'm dir_c/file3" >> dir_c/file3

chown administer:group_a dir_a
chown administer:group_b dir_b
chown administer:group_c dir_c
chmod 770 dir_a
chmod +t dir_a
chmod g+s dir_a

chmod 770 dir_b
chmod +t dir_b
chmod g+s dir_b

chmod 750 dir_c
