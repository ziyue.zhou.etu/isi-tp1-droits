# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Ziyue ZHOU ziyue.zhou.etu@univ-lille.fr

- Nom, Prénom, email: ___

## Question 1

Non.Parce que ce fichier n'est pas accessible en écriture pour l'utilisateur.

## Question 2

- on peut accéder à l'ensemble de ses sous-répertoires.

- Le groupe d'utilisateurs ubuntu où se trouve toto n'a pas l'autorisation d'exécuter le répertoire
  ```bash: cd: mydir: Permission denied  
  ```

- Le groupe d'utilisateurs ubuntu où se trouve toto n'a pas l'autorisation d'exécuter le répertoire, il ne peut donc pas accéder à ses sous-répertoires.

  ```
  ls: cannot access 'mydir/.': Permission denied
  ls: cannot access 'mydir/..': Permission denied
  ls: cannot access 'mydir/data.txt': Permission denied
  total 0
  d????????? ? ? ? ?            ? .
  d????????? ? ? ? ?            ? ..
  -????????? ? ? ? ?            ? data.txt
  ```


## Question 3
- Tous les ID sont 1001, l'utilisateur toto n'a pas l'autorisation de lire le fichier, le processus ne peut donc pas être exécuté
  ```
  toto@vm1:/home/ubuntu$ ./suid
  EUID: 1001
  EGID: 1001
  RUID: 1001
  RGID: 1001
  Cannot open file

  ```


- EUID devient 1001, les autres ID restent inchangés.L'utilisateur toto peut lire le fichier à ce moment, et le processus peut s'exécuter
  ```
  ubuntu@vm1:~$ chmod u+s suid
  toto@vm1:/home/ubuntu$ ./suid
  EUID is 1000
  EGID is 1001
  RUID is 1001
  RGID is 1001
  hello world
  ```


## Question 4

```
ubuntu@vm1:~$ chmod  u+s suid.py
toto@vm1:/home/ubuntu$ python3 suid.py
EUID:  1001
EGID:  1001

```

L'EUID n'a pas changé.

## Question 5

- **chfn** permet de change le nom et les informations d'un utilisateur.

Le fichier est lisable pour tout le monde, seul le propriétaire(root) dispose des autorisations d'écriture et d'exécution.

Le caractère s indique la permission d’exécution avec set-user-id. Ainsi, d'autres utilisateurs peuvent également exécuter le fichier

  ```
  toto@vm1:/home/ubuntu$ ls -al /usr/bin/chfn
  -rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn
  ```


-
  ```
  toto:x:1001:1001:,,,:/home/toto:/bin/bash
  // Après lancé chfn
  toto:x:1001:1001:,56,0666564400,0000000000:/home/toto:/bin/bash
  ```

## Question 6

Le mot de passe est stocké dans **/etc/shadow** et chiffré


## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests.
